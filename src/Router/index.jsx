import { BrowserRouter, Routes, Route } from "react-router-dom";
import SwitchPlantilla from "../SwitchPlantilla";

const Router = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route index element={<SwitchPlantilla />} />
			</Routes>
		</BrowserRouter>
	);
};

export default Router;
